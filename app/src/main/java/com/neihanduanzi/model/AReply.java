package com.neihanduanzi.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by 齐泽威 on 2016/11/1.
 */

/**
 * 全部评论实体类
 */
public class AReply {
    @SerializedName("recent_comments")
    private List<Reply> recent_comments;
    @SerializedName("top_comments")
    private List<Reply> top_comments;
    public List<Reply> getRecent_comments() {
        return recent_comments;
    }
    public List<Reply> getTop_comments() {
        return top_comments;
    }

    public static class Reply{
        @SerializedName("user_name")
        private String user_name;
        @SerializedName("text")
        private String text;
        @SerializedName("digg_count")
        private long digg_count;
        @SerializedName("avatar_url")
        private String avatar_url;

        public String getAvatar_url() {
            return avatar_url;
        }
        public String getUser_name() {
            return user_name;
        }
        public String getText() {
            return text;
        }
        public long getDigg_count() {
            return digg_count;
        }
    }
}
