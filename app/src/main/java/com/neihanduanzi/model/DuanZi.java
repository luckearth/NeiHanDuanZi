package com.neihanduanzi.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Administrator on 2016/10/31 0031.
 */

public class DuanZi {

    @SerializedName("data")
    private List<Data> data;


    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }
    public static class Data{

        @SerializedName("type")
        private int type;
        @SerializedName("group")
        private Group group;


        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public Group getGroup() {
            return group;
        }

        public void setGroup(Group group) {
            this.group = group;
        }

        public static class Group{
            @SerializedName("group_id")
            public long group_id;
            @SerializedName("text")
            public String text;
            @SerializedName("content")
            public String content;
            @SerializedName("share_count")
            public int share_count;
            @SerializedName("category_name")
            public String category_name;
            @SerializedName("digg_count")
            public int digg_count;
            @SerializedName("bury_count")
            public int bury_count;
            @SerializedName("user")
            public UserBean user;
            @SerializedName("comment_count")
            private int comment_count;
            @SerializedName("share_url")
            private String share_url;

            public String getShare_url() {
                return share_url;
            }

            public void setShare_url(String share_url) {
                this.share_url = share_url;
            }

            public long getGroup_id() {
                return group_id;
            }

            public void setGroup_id(long group_id) {
                this.group_id = group_id;
            }

            public int getComment_count() {
                return comment_count;
            }

            public void setComment_count(int comment_count) {
                this.comment_count = comment_count;
            }

            public String getContent() {
                return content;
            }

            public void setContent(String content) {
                this.content = content;
            }

            public int getShare_count() {
                return share_count;
            }

            public void setShare_count(int share_count) {
                this.share_count = share_count;
            }

            public String getCategory_name() {
                return category_name;
            }

            public void setCategory_name(String category_name) {
                this.category_name = category_name;
            }

            public int getDigg_count() {
                return digg_count;
            }

            public void setDigg_count(int digg_count) {
                this.digg_count = digg_count;
            }

            public int getBury_count() {
                return bury_count;
            }

            public void setBury_count(int bury_count) {
                this.bury_count = bury_count;
            }

            public UserBean getUser() {
                return user;
            }

            public void setUser(UserBean user) {
                this.user = user;
            }

            public String getText() {
                return text;
            }

            public void setText(String text) {
                this.text = text;
            }

            public static class UserBean {
                @SerializedName("user_id")
                private long user_id;
                @SerializedName("name")
                private String name;
                @SerializedName("followings")
                private int followings;
                @SerializedName("avatar_url")
                private String avatar_url;
                @SerializedName("followers")
                private int followers;
                @SerializedName("is_following")
                private boolean is_following;
                @SerializedName("user_verified")
                private boolean user_verified;

                public long getUser_id() {
                    return user_id;
                }

                public void setUser_id(long user_id) {
                    this.user_id = user_id;
                }

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }

                public int getFollowings() {
                    return followings;
                }

                public void setFollowings(int followings) {
                    this.followings = followings;
                }

                public String getAvatar_url() {
                    return avatar_url;
                }

                public void setAvatar_url(String avatar_url) {
                    this.avatar_url = avatar_url;
                }

                public int getFollowers() {
                    return followers;
                }

                public void setFollowers(int followers) {
                    this.followers = followers;
                }

                public boolean isIs_following() {
                    return is_following;
                }

                public void setIs_following(boolean is_following) {
                    this.is_following = is_following;
                }

                public boolean isUser_verified() {
                    return user_verified;
                }

                public void setUser_verified(boolean user_verified) {
                    this.user_verified = user_verified;
                }

            }
        }

    }



}
