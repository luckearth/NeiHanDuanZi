package com.neihanduanzi;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioGroup;

import com.neihanduanzi.fragment.CheckFragment;
import com.neihanduanzi.fragment.FoundFragment;
import com.neihanduanzi.fragment.IndexFragment;
import com.neihanduanzi.fragment.MessageFragment;
import com.neihanduanzi.model.DuanZi;
import com.neihanduanzi.model.ImageModel;
import com.neihanduanzi.model.RecommendBean;
import com.neihanduanzi.model.VideoModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.onekeyshare.OnekeyShare;

public class MainActivity extends AppCompatActivity implements RadioGroup.OnCheckedChangeListener, View.OnClickListener {
    private IndexFragment mIndexFragment;
    private FoundFragment mFoundFragment;
    private CheckFragment mCheckFragment;
    private MessageFragment mMessageFragment;
    private ImageView mUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ShareSDK.initSDK(this);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.main_tool_bar);
        mUser = (ImageView) findViewById(R.id.user);
        setSupportActionBar(toolbar);
        RadioGroup radioGroup = (RadioGroup) findViewById(R.id.main_radiogroup);
        mIndexFragment = new IndexFragment();
        mFoundFragment = new FoundFragment();
        mCheckFragment = new CheckFragment();
        mMessageFragment = new MessageFragment();
        EventBus.getDefault().register(this);

        radioGroup.setOnCheckedChangeListener(this);
        radioGroup.check(R.id.main_index);

        mUser.setOnClickListener(this);


    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction Transaction = manager.beginTransaction();
        switch (checkedId) {
            case R.id.main_index:
                Transaction.replace(R.id.main_fragment,mIndexFragment);
                break;
            case R.id.main_found:
                Transaction.replace(R.id.main_fragment,mFoundFragment);
                break;
            case R.id.main_check:
                Transaction.replace(R.id.main_fragment,mCheckFragment);
                break;
            case R.id.main_message:
                Transaction.replace(R.id.main_fragment,mMessageFragment);
                break;
        }
        Transaction.commit();
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(this, LoadActivity.class);
        startActivity(intent);
    }
//    public void showShare(){
//        showShare(null, null, null, null, null);
//    }

    /**
     * 每个参数有就填没有写null
     */
//    public void showShare
//            (String text, String imgUrl, String title, String titleUrl, String weixinUrl) {
//        ShareSDK.initSDK(this);
//        OnekeyShare oks = new OnekeyShare();
//        //关闭sso授权
//        oks.disableSSOWhenAuthorize();
//
//        // 分享时Notification的图标和文字  2.5.9以后的版本不调用此方法
//        //oks.setNotification(R.drawable.ic_launcher, getString(R.string.app_name));
//        // title标题，印象笔记、邮箱、信息、微信、人人网和QQ空间使用
//        if (title != null) {
//            oks.setTitle(title);
//        }else {
//            oks.setTitle("标题");
//        }
//        // titleUrl是标题的网络链接，仅在人人网和QQ空间使用
//        if (titleUrl != null){
//            oks.setTitleUrl(titleUrl);
//        }else {
//            oks.setTitleUrl("http://www.baidu.com");
//        }
//        // text是分享文本，所有平台都需要这个字段
//        if (text != null){
//            oks.setText(text);
//        }else {
//            oks.setText("分享文本");
//        }
//        //分享网络图片，新浪微博分享网络图片需要通过审核后申请高级写入接口，否则请注释掉测试新浪微博
//        if (imgUrl != null) {
//            oks.setImageUrl(imgUrl);
//        }else {
//            oks.setImageUrl("http://f1.sharesdk.cn/imgs/2014/02/26/owWpLZo_638x960.jpg");
//        }
//        // imagePath是图片的本地路径，Linked-In以外的平台都支持此参数
//        //oks.setImagePath("/sdcard/test.jpg");//确保SDcard下面存在此张图片
//        // url仅在微信（包括好友和朋友圈）中使用
//        if (weixinUrl != null){
//            oks.setUrl(weixinUrl);
//        }else {
//            oks.setUrl("http://sharesdk.cn");
//        }
//// 启动分享GUI
//        oks.show(this);
//    }
    
    //视频分享
    @Subscribe
   public void share(VideoModel.Fdata fdata){
        ShareSDK.initSDK(this);
        OnekeyShare oks = new OnekeyShare();
        oks.disableSSOWhenAuthorize();
        String title = fdata.getVideoDetial().getTitle();
        if (title != null) {
            oks.setTitle(title);
        }else {
            oks.setTitle("标题");
        }
        // titleUrl是标题的网络链接，仅在人人网和QQ空间使用
        if (fdata.getVideoDetial().getTitle() != null){
            oks.setTitleUrl(fdata.getVideoDetial().getTitle());
        }else {
            oks.setTitleUrl("http://www.baidu.com");
        }
        // text是分享文本，所有平台都需要这个字段
        if (fdata.getVideoDetial().getTitle() != null){
            oks.setText(fdata.getVideoDetial().getTitle());
        }else {
            oks.setText("分享文本");
        }
        //分享网络图片，新浪微博分享网络图片需要通过审核后申请高级写入接口，否则请注释掉测试新浪微博
        String url = fdata.getVideoDetial().getCover().getUrl_list().get(0).getUrl();
        if (url != null) {
            oks.setImageUrl(url);
        }else {
            oks.setImageUrl("http://f1.sharesdk.cn/imgs/2014/02/26/owWpLZo_638x960.jpg");
        }
        // imagePath是图片的本地路径，Linked-In以外的平台都支持此参数
        //oks.setImagePath("/sdcard/test.jpg");//确保SDcard下面存在此张图片
    
// 启动分享GUI
        oks.show(this);
    }
    
    //图片分享
    @Subscribe
    public void imageshare(ImageModel.Data fdata){
        Log.d("aaaa", "imageshare: ");
        ShareSDK.initSDK(this);
        OnekeyShare oks = new OnekeyShare();
        oks.disableSSOWhenAuthorize();
        String title = fdata.getGroup().getContent();
        if (title != null) {
            oks.setTitle(title);
        }else {
            oks.setTitle("标题");
        }
        // titleUrl是标题的网络链接，仅在人人网和QQ空间使用
        if (fdata.getGroup().getContent() != null){
            oks.setTitleUrl(fdata.getGroup().getContent());
        }else {
            oks.setTitleUrl("http://www.baidu.com");
        }
        // text是分享文本，所有平台都需要这个字段
        if (fdata.getGroup().getContent()!= null){
            oks.setText(fdata.getGroup().getContent());
        }else {
            oks.setText("分享文本");
        }
        //分享网络图片，新浪微博分享网络图片需要通过审核后申请高级写入接口，否则请注释掉测试新浪微博
        String url = fdata.getGroup().getLarge_image().getUrl_list().get(0).getUrl();
        if (url != null) {
            oks.setImageUrl(url);
        }else {
            oks.setImageUrl("http://f1.sharesdk.cn/imgs/2014/02/26/owWpLZo_638x960.jpg");
        }
        // imagePath是图片的本地路径，Linked-In以外的平台都支持此参数
        //oks.setImagePath("/sdcard/test.jpg");//确保SDcard下面存在此张图片

// 启动分享GUI
        oks.show(this);
    }
    
    //段子分享
    @Subscribe
    public void duanzi(DuanZi.Data fdata){
        Log.d("aaaa", "imageshare: ");
        ShareSDK.initSDK(this);
        OnekeyShare oks = new OnekeyShare();
        oks.disableSSOWhenAuthorize();
        String title = fdata.getGroup().getContent();
        if (title != null) {
            oks.setTitle(title);
        }else {
            oks.setTitle("标题");
        }
        // titleUrl是标题的网络链接，仅在人人网和QQ空间使用
        if (fdata.getGroup().getContent() != null){
            oks.setTitleUrl(fdata.getGroup().getContent());
        }else {
            oks.setTitleUrl("http://www.baidu.com");
        }
        // text是分享文本，所有平台都需要这个字段
        if (fdata.getGroup().getContent()!= null){
            oks.setText(fdata.getGroup().getContent());
        }else {
            oks.setText("分享文本");
        }
//        //分享网络图片，新浪微博分享网络图片需要通过审核后申请高级写入接口，否则请注释掉测试新浪微博
//        String url = fdata.getGroup().getLarge_image().getUrl_list().get(0).getUrl();
//        if (url != null) {
//            oks.setImageUrl(url);
//        }else {
//            oks.setImageUrl("http://f1.sharesdk.cn/imgs/2014/02/26/owWpLZo_638x960.jpg");
//        }
        // imagePath是图片的本地路径，Linked-In以外的平台都支持此参数
        //oks.setImagePath("/sdcard/test.jpg");//确保SDcard下面存在此张图片

// 启动分享GUI
        oks.show(this);
    }
    
    //推荐分享
    @Subscribe
    public void tuijian(RecommendBean fdata){
        Log.d("aaaa", "imageshare: ");
        ShareSDK.initSDK(this);
        OnekeyShare oks = new OnekeyShare();
        oks.disableSSOWhenAuthorize();
        String title = fdata.getTitle();
        if (title != null) {
            oks.setTitle(title);
        }else {
            oks.setTitle("标题");
        }
        // titleUrl是标题的网络链接，仅在人人网和QQ空间使用
        if (fdata.getTitle() != null){
            oks.setTitleUrl(fdata.getTitle());
        }else {
            oks.setTitleUrl("http://www.baidu.com");
        }
        // text是分享文本，所有平台都需要这个字段
        if (fdata.getTitle()!= null){
            oks.setText(fdata.getTitle());
        }else {
            oks.setText("分享文本");
        }
//        //分享网络图片，新浪微博分享网络图片需要通过审核后申请高级写入接口，否则请注释掉测试新浪微博
//        String url = fdata.getGroup().getLarge_image().getUrl_list().get(0).getUrl();
//        if (url != null) {
//            oks.setImageUrl(url);
//        }else {
//            oks.setImageUrl("http://f1.sharesdk.cn/imgs/2014/02/26/owWpLZo_638x960.jpg");
//        }
        // imagePath是图片的本地路径，Linked-In以外的平台都支持此参数
        //oks.setImagePath("/sdcard/test.jpg");//确保SDcard下面存在此张图片

// 启动分享GUI
        oks.show(this);
    }


    public void jumpLogin(View view) {
        Intent intent = new Intent(this,LoadActivity.class);
        startActivity(intent);
    }
}
