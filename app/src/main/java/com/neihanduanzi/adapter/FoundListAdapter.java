package com.neihanduanzi.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.neihanduanzi.R;
import com.neihanduanzi.model.FoundModel1;
import com.squareup.picasso.Picasso;

import java.util.List;



/**
 * Created by Administrator on 2016/10/31 0031.
 */

public class FoundListAdapter extends BaseAdapter {
    private Context mContext;
    private List<FoundModel1.Data.Categories.CategoryList> mItems;

    public FoundListAdapter(Context context, List<FoundModel1.Data.Categories.CategoryList> items) {
        mContext = context;
        mItems = items;
    }

    @Override
    public int getCount() {
        int ret = 0;
        if (mItems != null){
            ret = mItems.size();
        }
        return ret;
    }

    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View ret = null;
        if (convertView != null) {
            ret = convertView;
        }else {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            ret = inflater.inflate(R.layout.foundfragment_list, parent, false);
        }
        ViewHolder holder = (ViewHolder) ret.getTag();
        if (holder == null) {
            holder = new ViewHolder(ret);
            ret.setTag(holder);
        }

        FoundModel1.Data.Categories.CategoryList categoryList = mItems.get(position);
        holder.mTxtTitle.setText(categoryList.getName());

        holder.mTxtIntroduce.setText(categoryList.getIntro());
        String str = Integer.toString(categoryList.getSubscribe_count()) + " 订阅"
                + " | " + "总贴数";
        holder.mTxtCount.setText(str);

        String s = Integer.toString(categoryList.getTotal_updates());
        holder.mtxtNumber.setText(s);

        if (holder.mImageView != null) {
            String url = categoryList.getSmall_icon_url();
            if (url != null){
                Context context = holder.mImageView.getContext();
                Picasso.with(context)
                        .load(url)
                        .resize(120, 100)
                        .config(Bitmap.Config.ARGB_8888)
                        .into(holder.mImageView);
            }

        }
        return ret;
    }
    private static class ViewHolder{
        private ImageView mImageView;
        private TextView mTxtTitle;
        private TextView mTxtIntroduce;
        private TextView mTxtCount;
        private TextView mtxtNumber;
        ViewHolder(View itemView){
            mImageView = (ImageView) itemView.findViewById(R.id.found_list_img);
            mTxtTitle = (TextView) itemView.findViewById(R.id.found_title_txt);
            mTxtIntroduce = (TextView) itemView.findViewById(R.id.found_introduce_txt);
            mTxtCount = (TextView) itemView.findViewById(R.id.found_count_txt);
            mtxtNumber = (TextView) itemView.findViewById(R.id.found_number_txt);
        }

    }
}
