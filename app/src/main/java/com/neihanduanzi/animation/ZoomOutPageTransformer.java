package com.neihanduanzi.animation;

import android.annotation.SuppressLint;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;

public class ZoomOutPageTransformer implements ViewPager.PageTransformer
{
    private static final float MIN_SCALE = 0.85f;  //最小缩放系数
    private static final float MIN_ALPHA = 0.5f;

    @SuppressLint("NewApi")
    public void transformPage(View view, float position)
    {
        int pageWidth = view.getWidth();
        int pageHeight = view.getHeight();

        Log.e("TAG", view + " , " + position + "");

        //判断如果position参数是负无穷到-1的时候，也就是此时这个page划出了屏幕左边缘之外不可见得情况，直接设置为透明的
        if (position < -1)
        {
            view.setAlpha(0);

        } else if (position <= 1)
        {    // [-1,1],这个页面还在屏幕右边缘即将滑入我们的视线，谷歌把它当成了一个缩放系数来用;

            //缩放系数是处于MIN_SCALE和1之间的，然后下面的代码就是根据position位置参数的变化来进行page的水平移动效果
            float scaleFactor = Math.max(MIN_SCALE, 1 - Math.abs(position));


            float vertMargin = pageHeight * (1 - scaleFactor) / 2;
            float horzMargin = pageWidth * (1 - scaleFactor) / 2;
            if (position < 0)
            {
                view.setTranslationX(horzMargin - vertMargin / 2);     //设置水平移动效果
            } else
            {
                view.setTranslationX(-horzMargin + vertMargin / 2);
            }

            // Scale the page down (between MIN_SCALE and 1)
            view.setScaleX(scaleFactor);
            view.setScaleY(scaleFactor);

            // Fade the page relative to its size.
            view.setAlpha(MIN_ALPHA + (scaleFactor - MIN_SCALE)
                    / (1 - MIN_SCALE) * (1 - MIN_ALPHA));

        } else
        { // (1,+Infinity]
            // This page is way off-screen to the right.
            view.setAlpha(0);
        }
    }
}