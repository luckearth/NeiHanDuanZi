package com.neihanduanzi.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.neihanduanzi.R;
import com.neihanduanzi.adapter.PingLunAdapter;
import com.neihanduanzi.api.PingLunService;
import com.neihanduanzi.model.AReply;
import com.neihanduanzi.myview.FullyListView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * A simple {@link Fragment} subclass.
 */
public class PingLunFragment extends BaseFragment {
    
    private FullyListView mListView;
    private List<AReply.Reply> mReplies;
    private PingLunAdapter mAdapter;

    public PingLunFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
//        EventBus.getDefault().register(this);
        mReplies = new ArrayList<>();
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onDestroy() {
//        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Override
    public String getFragmentTitle() {
        return "评论";
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View ret = inflater.inflate(R.layout.fragment_ping_lun, container, false);
        mListView = (FullyListView)ret.findViewById(R.id.PingLun_list);
        mAdapter = new PingLunAdapter(mReplies,getContext());
        mListView.setAdapter(mAdapter);
        long group_id = getActivity().getIntent().getLongExtra("group_id", -1);
        onEvent(group_id);

        return ret;
    }
    
//    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(long groupId){
//        long id = groupId.getGroup_id();
//        Log.d("aa", "onEvent: " + id);
        mReplies.clear();
        Retrofit.Builder builder = new Retrofit.Builder();
        builder.baseUrl("http://isub.snssdk.com/");
        builder.addConverterFactory(ScalarsConverterFactory.create());
        Retrofit build = builder.build();
        PingLunService service = build.create(PingLunService.class);
        Call<String> call = service.getPingLun(groupId);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                String sTotal = response.body();
                try {
                    JSONObject Total = new JSONObject(sTotal);
                    JSONObject data = Total.getJSONObject("data");
                    String s = data.toString();
                    Gson gson = new Gson();
                    AReply mAreply = gson.fromJson(s, AReply.class);
                    List<AReply.Reply> list = mAreply.getRecent_comments();
                    mReplies.addAll(list);
                    mAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });

    }
    

}
