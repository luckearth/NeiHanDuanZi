package com.neihanduanzi.fragment;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.neihanduanzi.R;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class MessageFragment extends BaseFragment implements RadioGroup.OnCheckedChangeListener {

    private RadioGroup font_tab_bar;
    private TextView font_text_text1;
    private SharedPreferences mSharedPreferences;
    private View ret;

    public MessageFragment() {
        // Required empty public constructor
    }

    @Override
    public String getFragmentTitle() {
        return "消息";
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ret = inflater.inflate(R.layout.fragment_message, container, false);

        //获取文字的id
        font_text_text1 = (TextView)ret.findViewById(R.id.font_text_text1);

        //RadioGroup
        font_tab_bar = (RadioGroup)ret. findViewById(R.id.font_tab_bar);
        if (font_tab_bar != null) {
            font_tab_bar.setOnCheckedChangeListener(this);
        }

        mSharedPreferences = getContext().getSharedPreferences("FontSetting", MODE_PRIVATE);
        return ret;
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.font_tab_small_font:
                SharedPreferences.Editor editor = mSharedPreferences.edit();
                editor.putFloat("novelFont1", 11f)
                        .putFloat("novelFont2", 13f)
                        .putFloat("Font1", 13f)
                        .putFloat("Font2", 11f)
                        .putFloat("diagramFont1", 11f)
                        .putFloat("diagramFont2", 11f);
                editor.commit();
                font_text_text1.setTextSize(12);

                break;
            case R.id.font_tab_middle_font:
                SharedPreferences.Editor editor1 = mSharedPreferences.edit();
                editor1.putFloat("novelFont1", 16f)
                        .putFloat("novelFont2", 18f)
                        .putFloat("Font1", 18f)
                        .putFloat("Font2", 16f)
                        .putFloat("diagramFont1", 16f)
                        .putFloat("diagramFont2", 16f);
                editor1.commit();
                font_text_text1.setTextSize(16);
                break;
            case R.id.font_tab_large_font:
                SharedPreferences.Editor editor2 = mSharedPreferences.edit();
                editor2.putFloat("novelFont1", 20f)
                        .putFloat("novelFont2", 24f)
                        .putFloat("Font1", 24f)
                        .putFloat("Font2", 20f)
                        .putFloat("diagramFont1", 20f)
                        .putFloat("diagramFont2", 20f);
                editor2.commit();
                font_text_text1.setTextSize(18);

                break;
        }
    }
}
