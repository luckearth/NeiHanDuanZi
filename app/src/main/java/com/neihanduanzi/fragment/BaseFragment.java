package com.neihanduanzi.fragment;

import android.support.v4.app.Fragment;

public abstract class BaseFragment extends Fragment {
    
    public BaseFragment(){

    }

    public abstract String getFragmentTitle();

}
