package com.neihanduanzi.fragment.load;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.neihanduanzi.R;
import com.neihanduanzi.fragment.BaseFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class ContributeFragment extends BaseFragment {


    public ContributeFragment() {
        // Required empty public constructor
    }

    @Override
    public String getFragmentTitle() {
        return "投稿";
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_contribute, container, false);
    }

}
