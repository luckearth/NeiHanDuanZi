package com.neihanduanzi.fragment;


import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.neihanduanzi.FoundDetailActivity;
import com.neihanduanzi.R;
import com.neihanduanzi.adapter.FoundListAdapter;
import com.neihanduanzi.api.FoundService1;
import com.neihanduanzi.model.FoundModel1;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * A simple {@link Fragment} subclass.
 */
public class FoundFragment extends BaseFragment implements AdapterView.OnItemClickListener {
    private FoundListAdapter mAdapter;
    private FoundService1 mFoundService;
    private List<FoundModel1.Data.Categories.CategoryList> mList;
    private ListView mListView;

    public FoundFragment() {
        // Required empty public constructor
    }

    @Override
    public String getFragmentTitle() {
        return "发现";
    }

    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container,
                             Bundle savedInstanceState) {
        View ret = inflater.inflate(R.layout.fragment_found, container, false);

        mListView = (ListView) ret.findViewById(R.id.found_fragment_list);
        mListView.setOnItemClickListener(this);
        if (mListView != null) {
                if (mFoundService == null) {
                    Retrofit.Builder builder = new Retrofit.Builder();
                    builder.baseUrl("http://ic.snssdk.com/");
                    builder.addConverterFactory(ScalarsConverterFactory.create());
                    builder.addConverterFactory(GsonConverterFactory.create());
                    Retrofit retrofit = builder.build();
                    mFoundService = retrofit.create(FoundService1.class);
                }
                Call<FoundModel1> call = mFoundService.getInfo();
                call.enqueue(new Callback<FoundModel1>() {


                    @Override
                    public void onResponse(Call<FoundModel1> call, Response<FoundModel1> response) {
                        if (response.isSuccessful()) {
                            // 网络请求
                            FoundModel1 body = response.body();
                            mList = body.getData().getCategories().getCategory_list();
                            FoundModel1.Data.RotateBanner.Banners banners =
                                    body.getData().getRotate_banner().getBanners().get(0);
                            List<FoundModel1.Data.RotateBanner.Banners.BannerUrl.UrlListBean> url_list = banners.getBanner_url().getUrl_list();

                            // 设置头标题
                            LayoutInflater inflater1 = LayoutInflater.from(getContext());
                            View headerView = inflater1.inflate(R.layout.found_header, null, false);
                            // 设置监听防止listview的点击错乱
                            headerView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                }
                            });
                            TextView textView = (TextView) headerView.findViewById(R.id.found_item_headtxt);
                            String title = body.getData().getRotate_banner().getBanners().get(0).getBanner_url().getTitle();
                           // TextView textView = new TextView(getContext());
                            textView.setText(title);
                            ImageView imageView = (ImageView) headerView.findViewById(R.id.found_item_headimg);
                            String url = url_list.get(0).getUrl();
                            Context context = imageView.getContext();
                            Picasso.with(context)
                                    .load(url)
                                    .config(Bitmap.Config.ARGB_8888)
                                    .into(imageView);

                            mAdapter = new FoundListAdapter(getContext(), mList);
                            mListView.addHeaderView(headerView);
                            mListView.setAdapter(mAdapter);
                        }

                    }

                    @Override
                    public void onFailure(Call<FoundModel1> call, Throwable t) {
                        Toast.makeText(getContext(), "网络加载失败，请检查网络", Toast.LENGTH_SHORT).show();
                    }
                });
            }


        return ret;
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        //Log.d("测试", "到这里了: ");
        Intent intent = new Intent(getContext(), FoundDetailActivity.class);
        String share_url = mList.get(position).getShare_url();
        String name = mList.get(position).getName();
        intent.putExtra("name", name);
        intent.putExtra("url", share_url);
        intent.putExtra("count", mList.get(position).getSubscribe_count());
        intent.putExtra("updates", mList.get(position).getTotal_updates());
        intent.putExtra("intro", mList.get(position).getIntro());
        intent.putExtra("imgUrl", mList.get(position).getSmall_icon_url());

        startActivity(intent);

    }
}
