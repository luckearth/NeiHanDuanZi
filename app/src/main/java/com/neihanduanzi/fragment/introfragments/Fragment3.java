package com.neihanduanzi.fragment.introfragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.neihanduanzi.MainActivity;
import com.neihanduanzi.R;


public class Fragment3 extends Fragment implements View.OnClickListener {

	private TextView come;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View ret = inflater.inflate(R.layout.fragment_3, container, false);
		come = (TextView)ret.findViewById(R.id.come_in);
		come.setOnClickListener(this);
		return ret;
	}

	@Override
	public void onClick(View v) {
		Intent intent = new Intent(getContext(), MainActivity.class);
		startActivity(intent);

	}
}
