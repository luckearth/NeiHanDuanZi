# 仿内涵段子APK

<table><tr>
<td><img src="https://img.shields.io/badge/version%20-1.0.0-brightgreen.svg" alt="version 1.0.0" ／></td>
<td><img src="https://img.shields.io/badge/API-15%2B-brightgreen.svg" alt="API 15+"／></td>
<td><img src="https://img.shields.io/badge/QQ-779754469-red.svg" alt="QQ:779754469"／></td>
</tr></table>

## 主要贡献

[wuta0](https://git.oschina.net/wuta0)<br/>
[wsally](https://git.oschina.net/wsally)<br/>
[AckermanLx](https://git.oschina.net/AckermanLx)<br/>

## 关于

完整的基于Android的项目：高仿内涵段子，已经完成。很好的学习项目。

- 从头到尾，完整项目，几乎包含所有Android基础（四大组件，Fragment，自定义组件，动画，开发框架等等）适宜初学者快速上手。
- Material Design 设计风格。
- 最好用的网络访问框架Retrofit。
- 提供反编译的接口，你可以尝试自己写一个自己用。
- 巨友们，欢迎加入这个项目。



## 主页 / 发现页

基于Android的Material Design的设计规范设计的主页面，CardView显示单个条目。主要包括推荐，视频，图片（包括Gif）,段子四个部分。<br/>
发现页基于WebView混合开发。

<table><tr>
<td><img src="http://oqg7nynni.bkt.clouddn.com/neihan_tuijianpng.png" alt="推荐" width="250"／></td>
<td><img src="http://oqg7nynni.bkt.clouddn.com/neihan_find.png" alt="详情" width="250"／></td>
</tr></table>

## 详情页 / 段子页面

因为视频，推荐，图片为不同开发者实现，所以详情页为Fragment页面组装，评论区为通用Fragment。<br/>
所有数据均为内涵段子官方数据。

<table><tr>
<td><img src="http://oqg7nynni.bkt.clouddn.com/neihan_detial.png" alt="详情" width="250"／></td>
<td><img src="http://oqg7nynni.bkt.clouddn.com/neihan_duanzi.png" alt="段子" width="250"／></td>
</tr></table>

## 其他功能

因为职业道德问题，该APP暂不支持登录，评论，点赞等行为。
其他支持：

- 全局的字体调控。
- 支持下拉刷新。

## TODO

- 视频播放控制（全屏，进度条）。
- 测试优化，避免一些能避免的Crash。

## Last

希望你能加入我们，一起完美这个项目,哪怕只是简单的测试。<br/>
我的邮箱：qizewei@vip.qq.com




